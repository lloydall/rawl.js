;(function(){
	var obj = {
		"key" : "maincontainer",
		"properties": [["name", "div"],["view", "main"]],
		"attrs": [["id", ""],["class", "full container"]],
		"children": [
			{
				"key":"gdday",
				"properties": [["name","div"]],
				"attrs": [["id", "gdday"]],
				"children": {
					"model" : "goodNightModel",
					"key" : "gnight",
					"attrs": [["id", "gnight"]],
					"styleBindings":[["color", "goodnightMessage"]],
					"children": {
						"type":"text",
						"content":"good night",
						"bindings":[["content","goodnightMessage"]]

					}
				}
			},
			{
				"type":"text",
				"content":"good Morn to ye",
				"model" : "goodMorningModel",
				"bindings":[["content","goodnightMessage"]]
			},
			{
				"type":"text",
				"content":"good Night Again",
				"model" : "goodNightModel",
				"bindings":[["content","goodnightMessage"]]
			},
			"component",
			"component",
			"component"
		]//,
		//"bindings": [["p.id","modelAttribute"]]
	};
	window.sample = obj;
})();
