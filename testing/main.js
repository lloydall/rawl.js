//import 'testing/sample.js';
//import 'testing/componentSample.js';
import component from 'testing/componentHtml.html!text';
import plaintext from 'testing/plaintext.html!text';
import htmlSample from 'testing/sampleHtml.html!text';
import dm from 'src/rawl.js';

var plainObj = {
    type:'text',
    nodeValue:plaintext,
    children:[],
    key: 'plaintext'
};
var o = dm(htmlSample, component, plainObj );

console.log(o);
//window.newmodel = new dm.manager.models.goodNightModel[0]({ goodnightMessage : 'Filled' });
//window.newmodelTwo = new dm.manager.models.goodMorningModel[0]({ gm : 'gmonring' });
//window.newmodelTwo = new(o[2][1])();
//document.body.appendChild(o[0][0].prototype.tree[0].domNodeRef);

window.newView = new o();
window.rawl = dm;


const mouseEventBus = Backbone.extend({},Backbone.Events);
window.addEventListener('mousemove', (e)=>{ mouseEventBus.trigger('mousemove', e); });

var maxX = window.innerWidth;
var maxY = window.innerHeight;
var maxXY = maxX * maxY;

let modelProperties = {
  initialize: function () {
    this.listenTo(mouseEventBus, 'mousemove', this.mouseEventHandler, this );
  },
  mouseEventHandler: function (e){
    this.set('mouseX', e.clientX);
    this.set('mouseY', e.clientY);
    this.set('mouseXY', e.clientY * e.clientX);
  },
  analogues: [{
    "mouseX": { t: [0] },
    "bgRed": { x: function(v, m){ return 255/maxX * v; } }
  },
  {
    "mouseY": { t: [0] },
    "bgBlue": { x: function(v, m){ return 255/maxY * v; } }
  },
  {
    "mouseXY": { t: [0] },
    "bgGreen": { x: function(v, m){ return 255/maxXY * v; } }
  },
  {
    "bgBlue": {
      t: [0]
    },
    "bgRed": {
      t: [0]
    },
    "bgGreen": {
      t: [0]
    },
    "bgcolor": {
      x: function(v,m){
        return 'rgb(' +
          parseInt(m.get('bgRed')) +
          ',' +
          parseInt(m.get('bgGreen')) +
          ',' +
          parseInt(m.get('bgBlue')) +
          ')';
      }
    }
  }
  /*, {
    "number": {
      o: [1, 2, 3, 4]
    },
    "bgcolor": {
      x: ['red', 'blue', 'green', 'grey']
    }
  }*/]
};

rawl.extendFn( 'model', 'collectionName', modelProperties );



rawl.getEntity('collection','collectionName', 0).add([{ number: 1 }, { number: 2 }]);

//window.newCollection = new(o[1][0])([{WHAT : "foSho"}]);
document.body.appendChild(window.newView.el);

/*
analogues: [{
  "mouseX": {
    t: [
      255,
      510,
      765
    ]
    o:[[1,2,3,4], [501,502] ]
  },
  "bgcolor": {
    x: [
      function(x,m){ m.getreturn 'rgb('+x+',0,0)'; },
      function(x,m){ console.log(2); return 'rgb(255,'+(x-255)+',0)'; },
      function(x,m){ console.log(3); return 'rgb(255,255,'+(x-510)+')'; }
    ]
  },

},
{
  "mouseY": {

  },
  "bgcolor": {

  }
}
*/
