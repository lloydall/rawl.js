import example from './example.html'; // plaintext import via rollup-plugin-string
import rawl from '../../dist/rawl.min.js';

const Backbone = rawl.Backbone;
const mouseEventBus = Backbone.extend({},Backbone.Events);

window.addEventListener('mousemove', (e)=>{ mouseEventBus.trigger('mousemove', e); });

const maxX = window.innerWidth;
const maxY = window.innerHeight;
const maxXY = maxX * maxY;

let exampleViewClass = rawl(example);

window.rawl = rawl;

function twofivefive(n,d){
  return parseInt(255/d * n);
}

function initialize(){
  let exampleView = new exampleViewClass();

  let modelProperties = {
    initialize: function () {
      this.listenTo(mouseEventBus, 'mousemove', this.mouseEventHandler, this );
    },
    mouseEventHandler: function (e){
      let r = twofivefive(e.clientX, maxX);
      let g = twofivefive(e.clientY * e.clientX, maxXY);
      let b = twofivefive(e.clientY, maxY);

      this.set('rgb', [r,g,b] );
    },
    analogues: [
    {
      "rgb": {
        o: '*'
      },
      "bgcolor": {
        x: function(v,m){
          return 'rgb(' + (v||[0,0,0]).toString() + ')';
        }
      }
    }]
  };

  rawl.extendFn( 'model', 'exampleCollection', modelProperties );

  rawl.getEntity( 'collection','exampleCollection', 0).add([{ number: 1 }, { number: 2 }]);


  document.body.appendChild(exampleView.el);
}

window.onload = initialize;
