import json from 'rollup-plugin-json';
import babel from 'rollup-plugin-babel';
import string from 'rollup-plugin-string';
//import nodeResolve from 'rollup-plugin-node-resolve';
//import commonjs from 'rollup-plugin-commonjs';
//import legacy from 'rollup-plugin-legacy';

export default {
  entry: 'src/example.js',
  dest: 'index.min.js',
  plugins: [
    string({ include: '**/*.html' }),
    json(),
    babel()
  ],
  format: 'cjs',
  sourceMap: 'inline'
};
