# RAWL.JS #

The front end scaffolding tool.

### What??? ###

RAWL.JS is a [Backbone](http://backbonejs.org/)/[Exoskeleton](http://paulmillr.com/exoskeleton/) based tool for scaffolding the front end of an application from markup.

You can bundle from source using [rollup](http://rollupjs.org) to module standard of your choosing, just edit rollup.config.js in the root dir and run 'rollup -c'.

Everything is well annotated.

### Why??? ###

To give you an expressive way of linking your model's properties to the many extraneous display properties necessary to build UIs, all those listeners and methods that clutter your controller views. Or just to scaffold working prototypes for single page apps.

### Example ###

#### example.html ####
```
#!html

<div class="collectionContainer">
	{{+	"view" : "CollectionView",
      "collection" : "exampleCollection" }}
  <div class="colourTracker">
      {{+
        "backgroundColor" : "bgcolor"
      }}
  </div>
</div>
```


#### example.js ####
```
#!javascript

import example from './example.html'; // plaintext import via rollup-plugin-string
import rawl from '../../dist/rawl.min.js';

const Backbone = rawl.Backbone;
const mouseEventBus = Backbone.extend({},Backbone.Events);

window.addEventListener('mousemove', (e)=>{ mouseEventBus.trigger('mousemove', e); });

const maxX = window.innerWidth;
const maxY = window.innerHeight;
const maxXY = maxX * maxY;

let exampleViewClass = rawl(example);

window.rawl = rawl;

function twofivefive(n,d){
  return parseInt(255/d * n);
}

function initialize(){
  let exampleView = new exampleViewClass();

  let modelProperties = {
    initialize: function () {
      this.listenTo(mouseEventBus, 'mousemove', this.mouseEventHandler, this );
    },
    mouseEventHandler: function (e){
      let r = twofivefive(e.clientX, maxX);
      let g = twofivefive(e.clientY * e.clientX, maxXY);
      let b = twofivefive(e.clientY, maxY);

      this.set('rgb', [r,g,b] );
    },
    analogues: [
    {
      "rgb": {
        o: '*'
      },
      "bgcolor": {
        x: function(v,m){
          return 'rgb(' + (v||[0,0,0]).toString() + ')';
        }
      }
    }]
  };

  rawl.extendFn( 'model', 'exampleCollection', modelProperties );

  rawl.getEntity( 'collection','exampleCollection', 0).add([{ number: 1 }, { number: 2 }]);


  document.body.appendChild(exampleView.el);
}

window.onload = initialize;

```


### Contribution guidelines ###

Feel free.

By Laurie Loydall
