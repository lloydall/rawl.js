import nodeResolve from 'rollup-plugin-node-resolve';
import inject from 'rollup-plugin-inject';
import legacy from 'rollup-plugin-legacy';
import babel from 'rollup-plugin-babel';
import json from 'rollup-plugin-json';

export default {
  entry: 'src/rawl.js',
  dest: 'dist/rawl.min.js',
  plugins: [
    json(),
    babel(),
    nodeResolve({
      jsnext: true,
      main: true,
      browser: true
    }),
    legacy({
      'node_modules/exoskeleton/exoskeleton.js': 'Backbone'
    }),
    inject({
      exclude: 'node_modules/exoskeleton/exoskeleton.js',
      Backbone: 'exoskeleton'
    })
  ],
  moduleContext: { 'node_modules/exoskeleton/exoskeleton.js' : 'window' },
  format: 'es',
  sourceMap: 'inline',
};
