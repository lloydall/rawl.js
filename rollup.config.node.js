import nodeResolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';
import inject from 'rollup-plugin-inject';
import legacy from 'rollup-plugin-legacy';
import babel from 'rollup-plugin-babel';
import json from 'rollup-plugin-json';

export default {
  entry: 'src/rawl.js',
  dest: 'dist/rawlnode.min.js',
  plugins: [
    json(),
    babel(),
    nodeResolve({
      jsnext: true,
      main: true,
      browser: false
    }),
    replace({
      include: ['node_modules/backbone.nativeview/backbone.nativeview.js','node_modules/backbone.nativeajax/backbone.nativeajax.js'],
      'module.exports': 'factory(Backbone) //'
    }),
    inject({
      exclude: 'node_modules/exoskeleton/exoskeleton.js',
      Backbone: 'exoskeleton'
    }),
    legacy({
      'node_modules/exoskeleton/exoskeleton.js': 'exports'
    })
  ],
  moduleName: 'rawljs',
  moduleContext: { 'node_modules/exoskeleton/exoskeleton.js' : 'exports' },
  format: 'cjs',
  indent: true,
  sourceMap: 'inline',
};
