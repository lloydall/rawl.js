import Backbone, { rawlManager } from './backbonebs.js';
import { defaults, bindAll, bind } from './utils.js';

export default Backbone.Collection.extend({
  constructor: function(models, options){
    this.rawlNumber = this.executionOrder();

    if(options && options.viewOptions){
      this.viewOptions = options.viewOptions;
      this.view = options.view;
    }

    Backbone.Collection.apply(this, arguments);

    rawlManager.collections[this.key].push(this);
    rawlManager.orderedArray.push(this);

  }
});
