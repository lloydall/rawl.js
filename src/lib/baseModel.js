import { defaults, flatten, indexPair } from './utils.js';
import Backbone, { rawlManager } from './backbonebs.js';

const findObservable = (item, threshold, val, d = []) => {

  if(item === val || item === '*') return d;

  if(item instanceof Array){
    let inItem;

    if(threshold){
      inItem = item.findIndex((el, i) => (
        !isNaN(el) && (val <= el || (i === item.length-1 && val > el))
      ));
    }else{

      inItem = item.indexOf(val);
    }

    if(inItem !== -1){
      d.push(inItem);
      return d;
    }

    for(let i of item){
      findObservable(i, threshold, val, d);
    }

  }else{
    return false;
  }
};

const drill = ( executed, oc  )=>{ // oc = observedCoordinate
    return !(executed instanceof Array) || !oc.length ?
      executed :
      drill( executed[oc.shift()] || executed, oc );
};

export default Backbone.Model.extend({
  constructor: function(attr){
    this.rawlNumber = this.executionOrder();
    this.analogues = Object.create(this.analogues||[]);

    let analogueKeys = flatten(this.analogues.map((a) => Object.keys(a))),
        moreDefaults = indexPair(analogueKeys,new Array(analogueKeys.length));

    this.defaults = defaults(this.defaults||{}, moreDefaults);

    Backbone.Model.apply(this, arguments);

    if(this.collection && this.collection.view){
      this.modelCollectionView(this.collection.view);
    }
    rawlManager.models[this.key].push(this);
    rawlManager.orderedArray.push(this);

    //3.4.4.1) model constructor function -- create analogues array, init model,
    //add instance to rawlManager
  },
  modelCollectionView: function(view, options = {}){
    /*view, vopt, m, c*/
    let vopt = this.collection.viewOptions,
        el   = options.el || vopt.el.parentNode.appendChild(vopt.el.cloneNode(true));

    let obj = {
        el: el,
        model: this
      };

    obj.el.style.display = null;

    new view( obj );
  },
  /* 3.4.4.2) analogues explainer -- when we change an analogised property on a model to one of the
    observed or threshold (o|t) values, the other properties in the analogue object are set
    to the executed (x) value. I'm pretty happy with this but I think I've forgotten an idea
    I had to make it even better.

    consider naming non data model attributes with v_ prefix for neatness.

   eg.
    {
      "screenWidth" : { o: '*' }, // '*' is wildcard.
      "screenWidthText" : { x: function(v){ return 'screen width is ' + v + 'px'; } }
    },
    {
      "name" : { o: [null,void(0),''] },
      "display" : { x: 'none', o:[] }
    },
    {
      "screenWidth": {
        t: [ 400, 700, 1200 ]
      },
      "youreUsing" : {
        x: [ "a phone", "a tablet", "a desktop" ]
      }
    }
  */
  analogueChange: function(){

    let changed = Object.assign({},this.changed);

    for(let a of this.analogues){
      for(let c in changed){

        let analogueKeys = Object.keys(a),
            analogueValues = Object.values(a);

        let vi = analogueKeys.indexOf(c),
            v = analogueValues[vi],
            observedValueMap = vi === -1 ? void(0) :
              findObservable(v.o || v.t, (!v.o && !!v.t), changed[c]);
        /* 3.4.4.3) Create a map to the value in the observed values arrays */

        if( !observedValueMap ) continue;

        let l = analogueKeys.length;

        for(let k = 0; k<l; k++){
          if(!hasOwnProperty.call( analogueValues[k], 'x' )) continue;

          // 3.4.4.4) get the value to be set
          let executed = drill(analogueValues[k].x, observedValueMap);

          if(typeof executed === 'function') executed = executed(changed[c],this);

          this.set( analogueKeys[k], executed, { silent: true } );
          changed[analogueKeys[k]] = executed;
        }

      }
    }
    this.changed = changed;
  }
});
