import Backbone, { rawlManager } from './backbonebs.js';
import { defaults, bindAll, bind } from './utils.js';

export default Backbone.View.extend({
  constructor: function(options = {}){ // 3.6.3.1) This gets complicated
    this.rawlNumber = this.executionOrder();

    let key = options.model ? options.model.key : this.key,
        viewModel = this.tree[0].model; // 3.6.3.2) The viewModel is the model on this view's object in the tree.

    bindAll(this, 'getModel');

    this.setModel( // 3.6.3.3) Decide wether we're within the scope of another view model of the same name.
      ( options.model || ( viewModel? new viewModel() : null )),
      ( options.scopedModels || {} )
    );

    if(options.el) this.tree[0].clonedDomNodeRef = options.el;

    rawlManager.views[this.key].push(this); // 3.6.3.4) Add this view to the manager
    rawlManager.orderedArray.push(this);

    this.cloneTree(); // 3.6.3.5) clone and re-reference the entire dom tree from the current view downwards

    this.rebind() // 3.6.3.6) crystalize the references to the right nodes for our changeFn
        .deleteCloneTree(); // 3.6.3.7) remove the scaffold of cloned nodes (not sure why ???)

    if(typeof this.model==='object'){ // 3.6.3.8) force all the analogues to fire
      this.model.changed = this.model.attributes;
      this.changeFn(this.model);
    }

    Backbone.View.apply(this, arguments); // 3.6.3.9) make a backbone view :)

  },
  getModel: function(model){
    return model || this.model;
  },
  setModel: function(model, scopedModels){

    this.attachScoped( scopedModels, model);
    /* 3.6.3.3.1) We need to find all of the models that might be referenced as we drill further into the dom.
                  eg. you might set a 'User' model in the main view, and have the attributes from this model
                  in myriad places throughout the view and in a context dominated by another model eg. a model
                  for 'Address' in an address form but the first field is automatically populated with the user's
                  name.
    */

    if(!model) return;

    if(typeof this.model === 'object'){
      this.model.trigger('unset', this.model); //this.stopListening(this.model); /* stop listening to old model */
    }

    this.model = model;
    this.listenTo( (this.getModel()), 'change', this.changeFn );
    this.listenTo( (this.getModel()), 'unset', this.unset );


  },
  attachScoped : function(scopedModels, model){
    this.scopedModels = scopedModels || this.scopedModels;

    model || (model = this.model);

    if(!model) return; /* End here if no model */
    this.scopedModels[model.key] = this.getModel;

  },
  unset: function(m){
    this.stopListening(m);
    //if(this.scopedModels[m.key]) this.listenTo(this.scopedModels[m.key](), 'change', this.changeFn);
  },

  /* fix node reference and build the binding map with reference to the new dom tree (cloneTree) */
  rebind: function(){
    if(!this.bindings || !(this.bindings instanceof Array)) return this;
    this.rebound = this.bindings.reduce((mem, v) => {

      let k = v[v.length-1],
          m = v[1],
          model = mem[m] = mem[m] || {},
          group = mem[m][k] = mem[m][k] || [ ],
          o = { node : v[0](), a1: v[2] }; // 3.6.3.6.1) v[0]() === component.clonedDomNodeRef || component.domNodeRef

      group.push( o );
      v.length === 5 && (o.a2 = v[3]);
      return mem;
    }, {});

    return this;
  },

  /* handler for all our bindings */
  changeFn: function(m,opt){
    m.analogueChange(m);

    for(var k in m.changed){

      if(this.rebound && this.rebound[m.key] && this.rebound[m.key][k]){
        for(let o of this.rebound[m.key][k]){
          let accessor = o.a2 || o.a1,
              modified = o.a2 ? o.node[o.a1] : o.node;
          //console.log( o.a2, o.a1 );
          if(o.a1 === 'attributes' ){
            //console.log(o.node, accessor, m.changed[k]);
            o.node.setAttribute(accessor, m.changed[k]);
          }else{
            //console.log(modified, accessor, m.changed[k]);
            modified[accessor] = m.changed[k];
          }
        }
      }

    }

  },

  /* new collection handler */ //moved to baseModel to create own view
  /*cAddHandler: function(view, vopt, m, c){
      let obj = {
          el: vopt.el.parentNode.appendChild(vopt.el.cloneNode(true)),
          model: m
        };

      vopt = defaults( obj, vopt );
      vopt.el.style.display = null;
      let v = new view( vopt );

  },*/

  /* clones and re-references the entire dom tree from the current view downwards */
  cloneTree: function(){
    var i = 0,
        l = this.tree.length,
        com,
        childNodes,
        nextCom;

    for(;i<l;i++){
      /*
      3.6.3.5.1) this.tree looks like this:

      [(1), (1.2), (1.2.2), (1.2.2.1), (1.2.2.1.2), (1.2.2.1.1), (1.2.1), (1.2.1.1), (1.1)]

      Each number represents a node in our tree (see 3.1 for more details)
      */
      com = this.tree[i];
      nextCom = this.tree[i+1];
      if(i === 0){
        com.clonedDomNodeRef = com.clonedDomNodeRef || com.domNodeRef.cloneNode(true);
      }

      if(nextCom){
        if( nextCom.depth>com.depth ){
          childNodes = com.clonedDomNodeRef.childNodes;
        }else{
          var n = com.depth,
              dn = com.clonedDomNodeRef.parentNode;

          while(n>nextCom.depth){
            dn = dn.parentNode;
            n--;
          }
          childNodes = dn.childNodes;
        }

        nextCom.clonedDomNodeRef = childNodes[nextCom.eq];
        //console.log(nextCom.domNodeRef, nextCom.eq);
      }
      for( let childCom of com.children ){
        let blockModel;

        if(childCom.model && !childCom.isCollection){
          blockModel =
            (this.getModel||this.scopedModels[childCom.model.prototype.key]||parseInt)() ||
            new childCom.model();

          if(!childCom.isView  && childCom.isModel){
            this.attachScoped(this.scopedModels || {}, blockModel);
          }
        }else if(com.model){
          blockModel = (this.getModel||this.scopedModels[com.model.prototype.key]||parseInt)();
        }
        if(childCom.isView){
          let vopt = {
                el: com.clonedDomNodeRef.childNodes[childCom.eq],
                scopedModels: Object.assign({}, this.scopedModels)
              };
	        //console.log('vop', vopt);
          if(childCom.isCollection){

            let //listener = this.cAddHandler.bind( null, childCom.view, vopt),
                models = blockModel? blockModel.get(childCom.collection.prototype.key) || [] : [],
                collection = new childCom.collection(null, { view: childCom.view, viewOptions: vopt });

            //console.log(vopt.el);

            vopt.el.style.display = 'none';
            //console.log('ModelsforCollection', blockModel, models);
            //this.listenTo( collection, 'add', listener);
            collection.add(models);
          }else{

            if(blockModel) vopt.model = blockModel;
            new childCom.view(vopt);

          }
        }

      }

    }
    this.el = this.tree[0].clonedDomNodeRef;

    return this;
  },
  deleteCloneTree: function(){
    for(var c of this.tree){

      c.clonedDomNodeRef = null;
    }
  }
});
