const nonBindingKeys = new Set(['model', 'collection', 'bindings', 'styleBindings', 'properties', 'isView', 'key', 'view', 'nn']),
      styleKeys = new Set(),
      attributeKeys = new Set([
        'class', 'code', 'codebase', 'color', 'cols', 'colspan', 'content', 'contenteditable',
        'contextmenu', 'controls', 'coords', 'data', 'datetime', 'default', 'defer',
        'dir', 'dirname', 'disabled', 'download', 'draggable', 'dropzone', 'enctype', 'for', 'form',
        'formaction', 'headers', 'height', 'hidden', 'high', 'href', 'hreflang', 'http-equiv',
        'icon', 'id', 'ismap', 'itemprop', 'keytype', 'kind', 'label', 'lang', 'language',
        'list', 'loop', 'low', 'manifest', 'max', 'maxlength', 'media', 'method', 'min', 'multiple', 'muted',
        'name', 'novalidate', 'open', 'optimum', 'pattern', 'ping', 'placeholder', 'poster', 'preload',
        'radiogroup', 'readonly', 'rel', 'required', 'reversed', 'rows', 'rowspan', 'sandbox', 'scope', 'scoped',
        'seamless', 'selected', 'shape', 'size', 'sizes', 'span', 'spellcheck', 'src', 'srcdoc', 'srclang',
        'srcset', 'start', 'step', 'style', 'summary', 'tabindex', 'target', 'title', 'type', 'usemap',
        'value', 'width', 'wrap'
      ]),
      css2props = {
        background: "background", backgroundAttachment: "background-attachment", backgroundColor: "background-color",
        backgroundImage: "background-image", backgroundPosition: "background-position", backgroundRepeat: "background-repeat",
        border: "border", borderCollapse: "border-collapse", borderColor: "border-color", borderSpacing: "border-spacing",
        borderStyle: "border-style", borderTop: "border-top", borderRight: "border-right", borderBottom: "border-bottom",
        borderLeft: "border-left", borderTopColor: "border-top-color", borderRightColor: "border-right-color", borderBottomColor: "border-bottom-color",
        borderLeftColor: "border-left-color", borderTopStyle: "border-top-style", borderRightStyle: "border-right-style", borderBottomStyle: "border-bottom-style",
        borderLeftStyle: "border-left-style", borderTopWidth: "border-top-width", borderRightWidth: "border-right-width", borderBottomWidth: "border-bottom-width",
        borderLeftWidth: "border-left-width", borderWidth: "border-width", bottom: "bottom", captionSide: "caption-side",
        clear: "clear", clip: "clip", color: "color", content: "content", counterIncrement: "counter-increment",
        counterReset: "counter-reset", cursor: "cursor", direction: "direction", display: "display",
        emptyCells: "empty-cells", cssFloat: "float", font: "font", fontFamily: "font-family", fontSize: "font-size",
        fontSizeAdjust: "font-size-adjust", fontStretch: "font-stretch", fontStyle: "font-style", fontVariant: "font-variant", fontWeight: "font-weight",
        height: "height", left: "left", letterSpacing: "letter-spacing", lineHeight: "line-height", listStyle: "list-style", listStyleImage: "list-style-image",
        listStylePosition: "list-style-position", listStyleType: "list-style-type", margin: "margin", marginTop: "margin-top", marginRight: "margin-right",
        marginBottom: "margin-bottom", marginLeft: "margin-left", markerOffset: "marker-offset", marks: "marks", maxHeight: "max-height", maxWidth: "max-width",
        minHeight: "min-height", minWidth: "min-width", opacity: "opacity", orphans: "orphans",
        outline: "outline", outlineColor: "outline-color", outlineStyle: "outline-style", outlineWidth: "outline-width", overflow: "overflow",
        padding: "padding", paddingTop: "padding-top", paddingRight: "padding-right", paddingBottom: "padding-bottom",
        paddingLeft: "padding-left", page: "page", pageBreakAfter: "page-break-after", pageBreakBefore: "page-break-before",
        pageBreakInside: "page-break-inside", position: "position", quotes: "quotes", right: "right", size: "size",
        tableLayout: "table-layout", textAlign: "text-align", textDecoration: "text-decoration", textIndent: "text-indent", textShadow: "text-shadow",
        textTransform: "text-transform", top: "top", unicodeBidi: "unicode-bidi", verticalAlign: "vertical-align",
        visibility: "visibility", whiteSpace: "white-space", widows: "widows", width: "width", wordSpacing: "word-spacing",
        zIndex: "z-index",
      };

for (var nextKey in css2props) {
  styleKeys.add(nextKey);
}

const specialAssign = function (target, source) {
  'use strict';

  if ( target === undefined || target === null ) {
    throw new TypeError('Cannot convert undefined or null to object');
  }

  var output = Object(target);

  var bindings = output.bindings = output.bindings || [];

  for (var nextKey in source) {

    if(!nonBindingKeys.has(nextKey)){
      if(attributeKeys.has(nextKey)){
        bindings[bindings.length] = [ 'attributes', nextKey, source[nextKey] ];
      }else if(styleKeys.has(nextKey)){
        bindings[bindings.length] = [ 'style', nextKey, source[nextKey] ];
      }else{
        bindings[bindings.length] = [nextKey, source[nextKey]];
      }

    }else{
      if(output[nextKey] instanceof Array){
        output[nextKey] = output[nextKey].concat(source[nextKey]);
      }else{
        output[nextKey] = source[nextKey];
      }

    }
  }
  return output;
};

var parseTag = (function(){
  var attrRE = /([\w-]+)|['"]{1}([^'"]*)['"]{1}/g;
  var lookup = (Object.create) ? Object.create(null) : {};
  // create optimized lookup object for
  // void elements as listed here:
  // http://www.w3.org/html/wg/drafts/html/master/syntax.html#void-elements

  lookup.area = true;
  lookup.base = true;
  lookup.br = true;
  lookup.col = true;
  lookup.embed = true;
  lookup.hr = true;
  lookup.img = true;
  lookup.input = true;
  lookup.keygen = true;
  lookup.link = true;
  lookup.menuitem = true;
  lookup.meta = true;
  lookup.param = true;
  lookup.source = true;
  lookup.track = true;
  lookup.wbr = true;

  return function (tag) {
    var i = 0;
    var key;
    var res = {
      type: 'tag',
      name: '',
      voidElement: false,
      attrs: {},
      children: [],
      nodeValue: null
    };

    tag.replace(attrRE, function (match) {
      if (i % 2) {
        key = match;
      } else {
        if (i === 0) {
          if (lookup[match] || tag.charAt(tag.length - 2) === '/') {
            res.voidElement = true;
          }
          res.name = match;
        } else {
          res.attrs[key] = match.replace(/['"]/g, '');
        }
      }
      i++;
    });

    return res;
  };
})();

var parseText = (function(){
  var splitters = ['{{','}}'],
  obRE = /(\{\{|\}\})/g;

  var notwhitespace = function(s, ns){
    s.replace(/[^ \s]{1,}[\S ]{1,}/gm, function(m){
      ns = ns.concat(m);
    });
    return ns;
  };

  return function(current, textContent){

    let inside = false,
        cn;
    //console.log('textParsing', textContent, inside, current);

    for(let s of textContent.split(obRE)){
      let i = splitters.indexOf(s);

      if(i===-1){
        if(!inside){
          let ns = notwhitespace(s,'');

          if(ns){
            let ob = {
              type: 'text',
              nodeValue: ns,
              bindings: []
            };
            cn = (current.children || (current.children = [] )).push(ob) -1;
          }
        }else{
          if(s.charAt(0) === '+'){

            let props = new Function( 'return {'.concat(s.substr(1),'};') )();

            if(isNaN(cn)){
              current = specialAssign( current, props );
            }else{
              current.children || ( current.children = [] );
              current.children[cn] = specialAssign( current.children[cn], props );
              //console.log(current.children[cn], props, 'current');
            }
          }else{
            current.children || ( current.children = [] );
            if(s.indexOf(':') === -1){
              current.children.push( s.replace(/"|\s/g,'') );
            }else{
              current.children.push( new Function( 'return {'.concat(s,'};') )() );
            }
          }
        }
        continue;
      }
      inside = !i;
    }
  };
})();

var tagRE = /<(?:"[^"]*"['"]*|'[^']*'['"]*|[^'">])+>/g;//,
//parseRE = /\{\{(?:"[^"]*"['"]*|'[^']*'['"]*|[^\}\}])+\}\}/g;

var empty = Object.create ? Object.create(null) : {};

export default function (html, options) {
  options || (options = {});
  options.components || (options.components = empty);
  var result = [];
  var current;
  var level = -1;
  var arr = [];
  var byTag = {};
  var inComponent = false;

  html.replace(tagRE, function (tag, index) {
    if (inComponent) {
      //console.log('cmpChexk', current.name, tag, 'inComponent', tag !== ('</' + current.name + '>'))
      if (tag !== ('</' + current.name + '>')) {
        return;
      } else {
        inComponent = false;
      }
    }
    var isOpen = tag.charAt(1) !== '/';
    var start = index + tag.length;
    var nextChar = html.charAt(start);
    var parent;

    if (isOpen) {
      level++;

      current = parseTag(tag);
      if (current.type === 'tag' && options.components[current.name]) {
        current.type = 'component';
        inComponent = true;
      }

      if (!current.voidElement && !inComponent && nextChar && nextChar !== '<') {

        parseText( current, html.slice(start, html.indexOf('<', start)) );

      }

      byTag[current.tagName] = current;

      // if we're at root, push new base node
      if (level === 0) {
        result.push(current);
      }

      parent = arr[level - 1];

      if (parent) {
        (parent.children || (parent.children = [] )).push(current);
      }

      arr[level] = current;
    }

    if (!isOpen || current.voidElement) {
      if (current.voidElement && nextChar !== '<' && nextChar && arr[level]) {
        //void element, not sure this works properly
        parseText(arr[level], html.slice( start, html.indexOf('<', start) ) );
        level--;
      }else{
        level--;
        if (!inComponent && nextChar !== '<' && nextChar && arr[level]) {

          parseText(arr[level], html.slice( start, html.indexOf('<', start) ) );

        }
      }
    }
  });
  //console.log('end',result);
  return result;
}
