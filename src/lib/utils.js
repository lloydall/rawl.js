function indexPair(keys, values) {
  keys || (keys = []);
  var result = {},
  i = 0,
  length = keys.length;

  for (; i < length; i++) {
    if (values) {
      result[keys[i]] = values[i] || null;
    } else {
      result[keys[i][0]] = keys[i][1] || null;
    }
  }
  return result;
}

function flatten( arr ){
   return arr.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);
}

var dombuilder = (function(){

  var names = [0,'tag',2,'text',4,5,6,7,'commentNode','documentNode','documentTypeNode','documentFragmentNode'],
  typeMap = {
    0 : null,
    tag : 'createElement',
    2 : null,
    text : 'createTextNode',
    4 : null,
    5 : null,
    6 : null,
    7 : null,
    commentNode : 'createComment',
    documentNode : 'implementation.createDocument',
    documentTypeNode : 'implementation.createDocumentType',
    documentFragmentNode : 'createDocumentFragment'
  },
  argMap = {
    0: [],
    tag : ['name'],
    2 : [],
    text : ['nodeValue'],
    4 : [],
    5 : [],
    6 : [],
    7 : [],
    commentNode : ['nodeValue'],
    documentNode : ['namespaceURI', 'qualifiedNameStr', 'documentType' ],
    documentTypeNode : ['qualifiedNameStr', 'publicId', 'systemId'],
    documentFragmentNode : ['createDocumentFragment']
  },
  styleRE = /[\:\;\s]+?(?=[^\:\;\s])/; //this is a bit crappy.

  function parseStyle(style, str){
    var stylekv = str.split(styleRE),
    i = 0,
    l = stylekv.length;

    for(; i < l; i+=2 ){
      style[stylekv[i]] = stylekv[i+1];
    }
    return style;
  }

  return function(obj){
    var nodeFn = document[typeMap[obj.type]] || document[typeMap[names[obj.type]]],
    argRefs = argMap[obj.type] || argMap[names[obj.type]],
    args = new Array(argRefs.length),

    i = 0,
    l = argRefs.length;

    for(; i<l; i++){
      args[i] = obj[argRefs[i]];
    }

    var node = nodeFn.apply(document, args);
    var attrs = !(obj.attrs instanceof Array)? obj.attrs : indexPair(obj.attrs || []) ;

    for (var n in attrs) {
      var attr = attrs[n];
      if(n === 'style'){
        parseStyle(node.style, attr);
        continue;
      }
      node.setAttribute(n, attr);

    }
    return node;
  };
})();

function bindAll(object){

  if(!object) return console.warn('bindAll requires at least one argument.');

  var functions = Array.prototype.slice.call(arguments, 1);

  Object.assign(object, Object.getPrototypeOf(object));

  //console.log(object, Object.getPrototypeOf(object));

  if (functions.length === 0 || functions[0][0]==='!') {
    let excluded = functions;
    functions = [];
    for (var method in object) {
      if(hasOwnProperty.call(object, method)) {
        if( typeof object[method] == 'function' &&
            toString.call(object[method]) == "[object Function]" &&
            excluded.indexOf('!'+method) === -1 ) {
          functions.push(method);
        }
      }
    }
  }

  for(var i = 0; i < functions.length; i++) {
    var f = functions[i];
    object[f] = bind(object[f], object);
  }
}

function bind(func, context) {
  return function() {
    return func.apply(context, arguments);
  };
}

function defaults(obj){
  var length = arguments.length;
  if (length < 2 || obj === null) return obj;
  for (var index = 1; index < length; index++) {
    var source = arguments[index],
        keys = Object.keys(source),
        l = keys.length;

    for (var i = 0; i < l; i++) {

      var key = keys[i];
      if (obj[key] === void 0){
        obj[key] = source[key];
      }
    }
  }
  return obj;
}

function completeAssign(target, ...sources) {
  sources.forEach(source => {
    let descriptors = Object.keys(source).reduce((descriptors, key) => {
      descriptors[key] = Object.getOwnPropertyDescriptor(source, key);
      return descriptors;
    }, {});
    // by default, Object.assign copies enumerable Symbols too
    Object.getOwnPropertySymbols(source).forEach(sym => {
      let descriptor = Object.getOwnPropertyDescriptor(source, sym);
      if (descriptor.enumerable) {
        descriptors[sym] = descriptor;
      }
    });
    Object.defineProperties(target, descriptors);
  });
  return target;
}

function findExisting(entityArray, key){
  let result = entityArray.find((o) => {

      return o.prototype.key === key;
    });
  //if(result) console.log(result.prototype);
  return result;
}

function extend(obj) {
  Array.prototype.slice.call(arguments, 1).forEach(function(item) {
    for (var key in item) obj[key] = item[key];
  });
  return obj;
}

function getElementByMap(rootElement, map){
  let element = rootElement, l = map.length, i = 0, e;
  for(;i<l;i++){
    e = element.childNodes[map[i]];
    if(e.nodeType === 3 && !(/[^\S  \s]/.test(e.data))){
      map[i]++;
      i--;
    }else{
      element = e;
    }
  }
  return element;
}
export { indexPair, defaults, bind, bindAll, dombuilder, completeAssign, findExisting, extend, flatten, getElementByMap };
