import Backbone from 'exoskeleton';
import 'backbone.nativeajax';
import 'backbone.nativeview';

const rawlManager = {
  views:{ },
  models:{ },
  collections:{ }
};

Backbone.View = Backbone.NativeView;
Backbone.Ajax = Backbone.NativeAjax;

export { Backbone as default, rawlManager };
