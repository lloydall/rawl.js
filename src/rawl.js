import Backbone, { rawlManager } from './lib/backbonebs.js';
import htmlParse from './lib/htmlParse.js';
import { indexPair, defaults, bind, bindAll, dombuilder, completeAssign, findExisting, extend, getElementByMap } from './lib/utils.js';
import viewType from './lib/baseView.js';
import modelType from './lib/baseModel.js';
import collectionType from './lib/baseCollection.js';


const base = bind(function(){
  return Object.assign(Object.create(this), { properties: [], attrs: [], children: [], bindings: [] });
}, {
  key: null,
  model: undefined,
  collection : undefined,
  view: undefined,
  name: 'div',
  type: 1,
  isModel: false,
  isView: false,
  isCollection: false
});

const protos = {
  view: viewType,
  model: modelType,
  collection: collectionType
};

let executionOrder;

export default (function(){
  let myExport = { };

  rawlManager.rawlNumber = -1;
  rawlManager.orderedArray = [];

  executionOrder = bind(function(){
    return (this.rawlNumber += 1);
  }, rawlManager);

  myExport.fn = bind(function(...tmps){ // 0) rawl(...tmps) called
    rawlManager.rawlNumber = -1;
    rawlManager.orderedArray = [];

    if (!tmps[0]) return 'nothing to output';

    let i = 0,
        l = tmps.length,
        components = {};

    for(; i < l; i++ ){
      if( typeof tmps[i] === 'string' ){
        tmps[i] = htmlParse(tmps[i])[0];
      }

      let key = tmps[i].key = (tmps[i].key || tmps[i].view || tmps[i].collection);
      // 1) assign key from view or collection name
      components[key] = tmps[i];

    }

    let arg = tmps[0],
        ii = 0,
        done = 0,
        node = null,
        frag = null,
        childFrag = null,
        frags = [],
        levels = [],

        objects = [],
        cloneObjects = [],
        exportComponents = {},

        arrays = [],

        //--backbone arrays
        views = Object.values(rawlManager.views).map((a)=>a[0]),
        models = Object.values(rawlManager.models).map((a)=>a[0]),
        collections = Object.values(rawlManager.collections).map((a)=>a[0]);


    //console.log(views, models, collections);
    for(;done!==undefined;){ // 3) our DOM tree constructing loop.

      ii++;
      if(ii>2000){break;}

      //---------------------------------------------------//
      if(arg instanceof Array){
        levels[levels.length-1] = arg.length-1;
        arrays[arrays.length-1] = arg;
        arg = arg[arg.length-1];
        continue; // 3.1) if we're at an array of child nodes go to the rightmost one and step over loop.
        /*
         // Explaination: Imagine the diagram below represents the DOM tree we're about to walk,//
         // each number is a node and decimal points represent ancestry. Our loop will first    //
         // proceed to node numbered 2.3.2 via the rightmost elements in the tree.              //

              [       1,                                        2*              ]
            [  1.1,            1.2   ]    [  2.1,           2.2,            2.3*  ]
          [1.1.1, 1.1.2] [1.2.1, 1.2.2] [2.1.1, 2.1.2] [2.2.1, 2.2.2] [2.3.1, 2.3.2*]

          * = parsed by the time we've reached the rightmost extremity

        */
      }

      if(typeof arg === 'string'){
        arrays[arrays.length-1][levels[levels.length-1]] =
                                                     arg =
        Object.create(components[arg]); // 3.2) if referencing another component clone it and go to 3.3
      }

      if(arg instanceof Object){ // arg is a component ( component === representation of a domnode )

        if(!arg.parsed){

          let parentObj = (objects[levels.length-1] || base() );
          let parentCloneObj = ( cloneObjects[levels.length-1] || base() );

          arg.depth = arrays.length; // depth === number of ancestors
          arg.eq = levels[levels.length-1]; // eq === index within parent
          arg.mapTo = levels.slice();
          /*
            the two properties provide us a way of mapping the structure.
          */
          defaults(arg, base());

          let cloneArg = extend({}, arg, { children:[], bindings: JSON.parse(JSON.stringify(arg.bindings)) }); // cloneObjects for the AST. :'-(

          parentCloneObj.children[arg.eq] = cloneObjects[levels.length] = cloneArg;

          //console.log('newObj', arg.key, JSON.stringify(arg.bindings), arg.attrs||{});

          if(cloneArg.key){
            exportComponents[cloneArg.key] = cloneArg;
          }
          //console.log(JSON.stringify(arg.bindings));

          objects[levels.length] = // 3.3) this is where we build all the models and data bindings.
            createComponent( arg )
            .modelbuilder( models, parentObj.model ) // 3.4) build/attach a model constructor
            .collectionbuilder( collections, parentObj.collection, models ) // 3.5) build/attach a collection constructor
            .viewbuilder( views, parentObj.view ) // 3.6) build/attach a view constructor
            .bindingBuilder(); // 3.7) bind model properties to DOM node properties

          //console.log(JSON.stringify(arg.bindings));
          if(arg.children && arg.children.length){
            // 3.8) move to child nodes, parse the rest of this level of nodes on the way back up
            levels[levels.length] = 0; //set for consistency
            arrays[arrays.length] = [];
            arg = arg.children;
            continue;
          }
        }

        // 3.9) get the existing or create a new fragment for the level.
        frag = frags[levels.length] = frags[levels.length] || document.createDocumentFragment();
        node = arg.domNodeRef.parentNode ? arg.domNodeRef.cloneNode(true) : arg.domNodeRef; //prevent node theft :)

        if(childFrag){  node.appendChild(childFrag); childFrag = null; }

        frag.insertBefore(node, frag.firstChild); // 3.10) build fragment right to left

        if( levels[levels.length-1] > 0 ){

          levels[levels.length-1]--;
          /*
          3.11) if there are any siblings to the left, step left on the eq map.
                Then grab the component at that position.
          */
          arg = arrays[arrays.length-1][ levels[levels.length-1] ];
          frag = null;

          continue;

        }else{
          // 3.12) all children are parsed, step back into the parent component
          done = levels.pop();
          childFrag = frags.pop(); // store the current fragment with all childNodes
          arrays.pop();

          !!objects[levels.length] && (objects[levels.length].parsed = true);
          arg = objects[levels.length];

        }
      }
    }
    //console.log(exportComponents);
    this.fn.manager.components = exportComponents;

    this.fn.manager.views = views.reduce((mem, v) => { // 4)
      let k = v.prototype.key;
      mem[k] = mem[k] || [];
      mem[k].push(v);
      return mem;
    }, {});

    this.fn.manager.models = models.reduce((mem, m) => { // 5)
      let k = m.prototype.key;
      mem[k] = mem[k] || [];
      mem[k].push(m);
      return mem;
    }, {});

    this.fn.manager.collections = collections.reduce((mem, v) => { // 6)
      let k = v.prototype.key;
      mem[k] = mem[k] || [];
      mem[k].push(v);
      return mem;
    }, {});
    return views[0];
  }, myExport);

  myExport.fn.manager = rawlManager; // 7)

  myExport.fn.newFn = function newFn(mcv, k, options){
    let params = {};
    this.manager[(mcv + 's')][k] = this.manager[(mcv + 's')][k] || [];
    params.key = k;
    params.executionOrder = executionOrder;
    params._newFn = true;
    if(mcv === 'collection'){
      let m = this.manager.models[k];
      params.model = m? m[0] : newFn.call(this, 'model', k, params);
    }

    options = extend(options, params);
    return (this.manager[(mcv + 's')][k][0] = protos[mcv].extend(options));
  };

  myExport.fn.getFn = function(mcv, k){ // 8)
    return this.manager[(mcv + 's')][k][0];
  };

  myExport.fn.extendBaseFn = function(mcv, options){
    extend(protos[mcv].prototype, options);
  };

  myExport.fn.clearInstances = function(){
    let vs = this.manager.views,
        ms = this.manager.models,
        cs = this.manager.collections;

    for(let k in vs){
      vs[k] = [vs[k][0]];
    }
    for(let k in ms){
      ms[k] = [ms[k][0]];
    }
    for(let k in cs){
      cs[k] = [cs[k][0]];
    }
    this.manager.rawlNumber = -1;
  };

  myExport.fn.extendFn = function(mcv, k, options){ // 9)
    let ent = this.manager[(mcv + 's')][k][0];
    extend(ent.prototype, options);
    return ent;
  };

  myExport.fn.getEntity = function(mcv, k, i){ // 10)
    return this.manager[(mcv + 's')][k][1+i];
  };

  myExport.fn.getElementByMap = getElementByMap;

  myExport.fn.Backbone = Backbone;

  extend(myExport.fn, Backbone.Events);

  return myExport.fn; // 11) return our main factory
})();

function createComponent(arg){
  /* 3.3.1) fill in defaults on dom node object, assign index, convert array of
    pairs named arg.properties to keys and values on the component. This array
    can be used, if you're building markup with objects, to keep information
    about the node neatly contained and keep the js template files more
    horizontal in aspect.

    eg.
    {
      model: undefined,
      collection : undefined,
      view: undefined,
      name: 'div',
      type: 1
    }
    vs
    {
      properties: [["model", undefined], ["collection ", undefined], ["view", undefined], ["name", 'div'], ["type", 1]
    }

  */
  let component = Object.assign( arg, myMethods, indexPair(arg.properties));
  bindAll(component, '!model', '!collection', '!view'); // 3.3.2) bindAll on component with negation instead of picking
  component.domNodeRef = dombuilder(component); // 3.3.3) get the actual DOM node prototype !! ( consider renaming domNodeRef to domNodePrototype)
  component.clonedDomNodeRef = null; // (??? consider renaming clonedDomNodeRef to domNode)
  return component;
}

const myMethods = {
  getDomNodeRef: function () {
    return this.clonedDomNodeRef || this.domNodeRef ; // 3.6.3.6.1.1) "|| this.domNodeRef" not sure about this.
  },
  bindingBuilder: function () {
    if (!!this.model && !!this.bindings) { //3.7.1) the binding builder adds bindings (model attribute -> DOM node attribute) to the current view's scope
      let bindTo = this.view.prototype;
      bindTo.bindings = (bindTo.bindings || [])
        .concat(this.bindings.map(
          (r) => {
            typeof r[0] !== 'function' && r.unshift(this.getDomNodeRef, this.model.prototype.key);
            return r;
          })
        );
    }

    return this;
  },

  collectionbuilder: function (collections, current, models) {
    let collectionKey = this.collection,
        existing = findExisting(collections, collectionKey);

    if (typeof collectionKey === 'function') return this; //escape here if we need to.

    if (!collectionKey) {
      if (current){
        this.collection = current;// 3.5.1) if there is no existing collection constructor the collection constructor === the parent component's collection constructor.
      }
      return this;
    }

    if (typeof existing === 'function') {
      this.collection = existing; // 3.5.2) if a collection constructor of same name was found, use it
    } else {
      collections[collections.length] = this.collection = collectionType.extend({
        model: this.model,
        key: collectionKey,
        executionOrder: executionOrder
      });// 3.5.3) else create collection constructor add to collection array
    }

    this.isCollection = true;
    return this;
  },

  modelbuilder: function (models, current) {

    if (this.collection && !this.model) {
      /*
        3.4.1) if the component specifies collection and not model, set up a
               model with the collection name as key.
      */
      this.model =
        typeof this.collection === 'function' ? this.collection.prototype.key : this.collection;
    }

    let modelKey = this.model,
        existing = findExisting(models,modelKey);

    if (typeof modelKey === 'function') return this; //escape here if we need to.

    if (!modelKey) {
      if(current) this.model = current; // 3.4.2) if there is no existing model constructor the model constructor === the parent component's model constructor.
      return this;
    }

    if (typeof existing === 'function') {
      this.model = existing; // 3.4.3) if a model constructor of same name was found, use it.
    } else {
      models[models.length] = this.model = modelType.extend({
        key: modelKey,
        executionOrder: executionOrder
      }); // 3.4.4) else create model constructor add to model array
    }
    this.isModel = true;
    //console.log(typeof this.model, modelKey, 'MODEL BUIDLER');
    return this;
  },

  viewbuilder: function (views, current) {

    let viewKey = this.view;

    if(viewKey === 'tracks'){ console.log('TRACKKKCKCKCKS'); }

    if (typeof viewKey === 'function') return this;

    if (!viewKey) {
      // 3.6.1) if no view specified add component to current view's tree. !important bit
      this.view = current;
      current.prototype.tree.push(this);
      return this;
    }

    let existing = findExisting(views,viewKey);

    if (typeof existing === 'function') {
      this.view = existing; // 3.6.2) if a view constructor of same name was found, use it.
    } else {
      views[views.length] = this.view = viewType.extend({
        // model: this.model,
        key: viewKey,
        executionOrder: executionOrder
        // tree: new Array(this)
      }); // 3.6.3)  else create view constructor add to view array
    }

    extend( this.view.prototype, { tree: [this], model: this.model,  });
    this.isView = true;
    return this;
  }
};

//export { Backbone };
